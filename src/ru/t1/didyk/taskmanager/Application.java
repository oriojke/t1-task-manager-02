package ru.t1.didyk.taskmanager;

import static ru.t1.didyk.taskmanager.constant.TerminalConst.*;

public class Application {

    public static void main(String[] args) {
        processArguments(args);
    }

    public static void processArguments(String[] args) {
        if (args == null || args.length == 0) return;
        final String argument = args[0];
        processArgument(argument);
    }

    public static void processArgument(String argument) {
        if (argument == null || argument.isEmpty()) return;
        switch (argument) {
            case HELP:
                showHelp();
                break;
            case ABOUT:
                showAbout();
                break;
            case VERSION:
                showVersion();
                break;
            default:
                showError();
                break;
        }
    }

    public static void showError() {
        System.err.println("[ERROR]");
        System.err.println("This argument is not supported.");
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("name: Nikolay Didyk");
        System.out.println("email: didyk@vtb.ru");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.2.0");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - Show developer info.\n", ABOUT);
        System.out.printf("%s - Show application version.\n", VERSION);
        System.out.printf("%s - Show command list.\n", HELP);
    }
}
